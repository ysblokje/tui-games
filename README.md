tui-games
=========

Some experiments with imtui and games.
This is just a project for myself to get an understanding for the whole direct
rendering stuff like imgui / imtui.

Getting sources
---------------

```
git clone https://gitlab.com/ysblokje/tui-games.git --recurse-submodules
``` 

Compilation
-----------

```
cd <sourcefolder>
mkdir build
cd build
cmake ../
cmake --build .
```

Running
-------

The executable(s) should be in the bin folder.
```
./bin/tetris
```

Thanks
------

Thanks goes to the people who mode imgui and imtui.
Also to the Lone Coder who made a video on creating tetris on the console
from which I stole... I mean borrowed most of the game code.


Ys
