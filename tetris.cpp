#include "imgui/imgui.h"
#include <chrono>
#include <thread>
#include <cstdint>
#include <iostream>
#include <array>
#include <vector>

#include <imtui/imtui.h>
#include <imtui/imtui-impl-ncurses.h>
#include <curses.h>



using namespace std;

wstring const tetromino[7] {
    L"..X."
    L"..X."
    L"..X."
    L"..X.",

    L"..X."
    L".XX."
    L".X.."
    L"....",

    L".X.."
    L".XX."
    L"..X."
    L"....",

    L"...."
    L".XX."
    L".XX."
    L"....",

    L"..X."
    L".XX."
    L"..X."
    L"....",

    L"...."
    L".XX."
    L"..X."
    L"..X.",

    L"...."
    L".XX."
    L".X.."
    L".X..",
};

constexpr int nFieldWidth = 12;
constexpr int nFieldHeight = 18;
using lineArray = std::array<uint8_t, nFieldWidth>;
std::array<lineArray, nFieldHeight> field;

int rotate(int px, int py, int r) {
    switch(r % 4) {
        case 0: return (py * 4) + px;           // 0°
        case 1: return 12 + py - (px * 4);     // 90°
        case 2: return 15 - (py * 4) - px;     // 180°
        case 3: return 3 - py + (px * 4);      // 270°
    }
    return 0;
}

void initField() {
    for(int x{}; x < nFieldWidth; ++x) {
        for(int y{}; y < nFieldHeight; ++y) {
            field[y][x] = (x == 0 || x == nFieldWidth -1 || y == nFieldHeight -1) ? 9 : 0;
        }
    }
}

bool doesFit(int ntetromino, int rotation, int posx, int posy) {
    for(int px{}; px < 4; ++px) {
        for(int py{}; py < 4; ++py) {
            // index;
            int pi = rotate(px, py, rotation);

            // field index
            int fi = field[posy + py][posx + px];

            if(posx + px >= 0 && posx + px < nFieldWidth) {
                if(posy + py >= 0 && posy + py < nFieldHeight) {
                    if(tetromino[ntetromino][pi] == L'X' && fi != 0) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}


int main() {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    auto screen = ImTui_ImplNcurses_Init(false);
    ImTui_ImplText_Init();
    auto maxX = getmaxx(stdscr);
    auto maxY = getmaxy(stdscr);
    bool bkey[4]={};
    bool gameover{false};
    int currentpiece{0};
    int currentrotation{0};
    int currentx = nFieldWidth / 2;
    int currenty{0};
    int speedcount{0};
    int speed{20};
    int piececount{0};
    int score{0};
    bool forcedown{false};
    bool rotatehold{true};
    vector<int> lines;
    auto previous = std::chrono::steady_clock::now();

    initField();

    while(!gameover) {
        auto hasinput = ImTui_ImplNcurses_NewFrame();
        auto check_input = [&]() { 
            rotatehold = true;
            bool direct_processing{false};
            if(hasinput) {
                auto input = ImGui::GetIO();
                if(ImGui::IsKeyDown(ImGui::GetKeyIndex(ImGuiKey_LeftArrow)) && 
                        doesFit(currentpiece, currentrotation, currentx - 1, currenty)) {
                    --currentx;
                }
                if(ImGui::IsKeyDown(ImGui::GetKeyIndex(ImGuiKey_RightArrow)) &&
                        doesFit(currentpiece, currentrotation, currentx + 1, currenty)) {
                    ++currentx;
                }
                if(ImGui::IsKeyDown(ImGui::GetKeyIndex(ImGuiKey_DownArrow)) &&
                        doesFit(currentpiece, currentrotation, currentx, currenty + 1)) {
                    ++currenty;
                }
                if(ImGui::IsKeyDown(ImGui::GetKeyIndex(ImGuiKey_UpArrow))) {
                    currentrotation += (rotatehold && 
                            doesFit(currentpiece, currentrotation + 1, currentx, currenty)) ? 1 : 0;
                    rotatehold = false;
                } 
            }
        };

        // Timing
        auto now = std::chrono::steady_clock::now();
        std::chrono::duration<float> diff = now - previous;
        if(!lines.empty()) {
            // ~ 400ms
            if(diff.count() > 0.4) {
                for(auto &v : lines) {
                    for(int px{1}; px < nFieldWidth -1; ++px) {
                        for(int py{v}; py > 0; --py) {
                            field[py][px] = field[py-1][px];
                        }
                        field[0][px] = 0;
                    }
                }
                lines.clear();
                previous = now;
            }
        } else {
            check_input();
            // ~ 50ms
            if(diff.count() > 0.02) {
                previous = now;
                ++speedcount;
                forcedown = speedcount == speed;

                if(forcedown) {
                    speedcount = 0;
                    ++piececount;

                    if(piececount % 50 == 0 && speed >= 10)
                        --speed;

                    if(doesFit(currentpiece, currentrotation, currentx, currenty + 1)) {
                        ++currenty;
                    } else {
                        for(int px{}; px < 4; ++px) {
                            for(int py{}; py < 4; ++py) {
                                if(tetromino[currentpiece][rotate(px, py, currentrotation)] != L'.')
                                    field[currenty + py][currentx + px] = currentpiece + 1;
                            }
                        }

                        for(int py{}; py < 4; ++py) {
                            if(currenty + py < nFieldHeight - 1) {
                                bool line{true};
                                for(int px{1}; px < nFieldWidth - 1; ++px) {
                                    line &= field[currenty + py][px] != 0;
                                }

                                if(line) {
                                    for(int px{1}; px < nFieldWidth - 1; ++px) {
                                        field[currenty + py][px] = 8;
                                    }
                                    lines.push_back(currenty + py);
                                }
                            }
                        }

                        score += 25;
                        if(!lines.empty()) {
                            score += (1 << lines.size()) * 100;
                        }

                        currentx = nFieldWidth / 2;
                        currenty = 0;
                        currentrotation = 0;
                        currentpiece = rand() % 7;

                        gameover = !doesFit(currentpiece, currentrotation, currentx, currenty);
                    }
                }
            }
        }
        // Render
        ImTui_ImplText_NewFrame();
        ImGui::NewFrame();

        ImGui::SetNextWindowPos(ImVec2(0.0, 0.0), ImGuiCond_Once);
        ImGui::SetNextWindowSize(ImVec2(maxX, maxY), ImGuiCond_Once);
        ImGui::Begin("Tetris", nullptr, 
                ImGuiWindowFlags_NoMove | 
                ImGuiWindowFlags_NoResize);

        // Draw playing field.
        for(auto line : field) {
            for(unsigned x{}; x < line.size(); ++x) {
                line[x] = L" ABCDEFG=#"[line[x]];
            }
            auto new_line=reinterpret_cast<const char*>(line.data());
            ImGui::TextUnformatted(new_line, new_line + line.size());
        }
        // Draw tetromino
        char tmp_char[2];
        tmp_char[0] = currentpiece + 'A';
        tmp_char[1] = 0;

        for(int px{} ; px < 4; ++px) {
            for(int py{}; py < 4; ++py) {
                if(tetromino[currentpiece][rotate(px, py, currentrotation)] == L'X') {
                    ImGui::SetCursorPos(ImVec2(currentx + px, currenty + py));
                    ImGui::TextUnformatted(reinterpret_cast<const char*>(tmp_char));
                }
            }
        }

        ImGui::SetCursorPos(ImVec2(nFieldWidth + 6, 2));
        ImGui::Text("Score %8d", score);

        ImGui::End();
        ImGui::Render();
        ImTui_ImplText_RenderDrawData(ImGui::GetDrawData(), screen);
        ImTui_ImplNcurses_DrawScreen();
    }


    ImTui_ImplText_Shutdown();
    ImTui_ImplNcurses_Shutdown();

    std::cout << "Game Over!! Score : " << score << "\n";
    return 0;
}
