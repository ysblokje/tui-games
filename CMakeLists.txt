cmake_minimum_required(VERSION 3.10)
project(Tetris)

set(EXECUTABLE_OUTPUT_PATH bin)

add_subdirectory(imtui)

add_executable(tetris tetris.cpp)
target_compile_features(tetris INTERFACE cxx_std_17)
target_link_libraries(tetris PRIVATE imtui-ncurses)

